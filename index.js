const {
  initializeApp,
  applicationDefault,
  cert,
} = require("firebase-admin/app");
const serviceAccount = require("./serviceAccount.json");
const {
  getFirestore,
  Timestamp,
  FieldValue,
} = require("firebase-admin/firestore");
require("dotenv").config();
const projectId = "draw-n-guess-6c513";
const app = initializeApp({
  credential: cert(serviceAccount),
  projectId: process.env.PROJECT_ID,
});
const db = getFirestore(app);

const io = require("socket.io")(5555, {
  cors: {
    origin: "https://draw-n-guess-server.herokuapp.com/",
  },
});

io.on("connection", (socket) => {
  // console.log(socket.id);
  socket.on("joinRoom", async (isNew, roomUid, userName, cb) => {
    socket.join(roomUid);
    console.log(roomUid);
    if (!isNew) {
      const roomRef = await db.collection("rooms").doc(roomUid).get();
      if (roomRef.exists) {
        // this means this player is joining an existing room
        const playerDrawing = roomRef.data().createdBy;
        cb({ status: "success", roomUid, playerDrawing });
      }
    } else {
      cb({ status: "success", roomUid, playerDrawing: userName });
    }
  });
  socket.on("finishedDrawing", async (roomUid, roundId, cb) => {
    console.log("round id", roundId);
    const roundData = await db
      .collection("rooms")
      .doc(roomUid)
      .collection("rounds")
      .doc(roundId)
      .get();
    if (roundData.exists) {
      const data = roundData.data();
      const { correctAnswer, playerDrawing } = data;
      console.log(data);
      const res = {
        correctAnswer,
        playerDrawing,
        roundId,
      };
      socket.broadcast.emit("playerFinishedDrawing", res);
      cb({ roundData: { correctAnswer, roundId, playerDrawing } });
    }
  });
});
